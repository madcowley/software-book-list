# Software book list

A collection of books recommended by the [Suncoast Devs community](https://suncoast.io/) and others.

- Working Effectively with Legacy Code, by Michael Feathers
- Domain-Driven Design
- Growing Object-Oriented Software
- Designing Data-Intensive Applications, by Martin Kleppman
- Evolutionary Architecture
- anything by Sandi Metz
- The Pragmatic Programmer
- Code Complete: A Practical Handbook of Software Construction
- [Learn You a Haskell for Great Good](http://learnyouahaskell.com/)
